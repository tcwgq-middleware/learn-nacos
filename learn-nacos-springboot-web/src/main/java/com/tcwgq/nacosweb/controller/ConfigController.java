package com.tcwgq.nacosweb.controller;

import com.tcwgq.nacosweb.feign.ConfigClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2022/9/4 18:04
 */
@RestController
@RequestMapping("/user")
public class ConfigController {

    @Autowired
    private ConfigClient configClient;

    @GetMapping("/info")
    public String user() {
        return configClient.user();
    }

}
