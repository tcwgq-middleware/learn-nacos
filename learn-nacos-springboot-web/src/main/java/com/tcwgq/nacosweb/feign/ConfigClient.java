package com.tcwgq.nacosweb.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tcwgq
 * @since 2022/9/11 11:11
 */
@FeignClient(name = "nacos-service")
@RequestMapping("/service/config")
public interface ConfigClient {
    @GetMapping("/user")
    String user();

}
