package com.tcwgq.simple_demo;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;

/**
 * @author tcwgq
 * @since 2022/9/4 15:45
 */
public class Demo1 {
    public static void main(String[] args) throws NacosException {
        // 不写命名空间，默认使用public
        ConfigService configService = NacosFactory.createConfigService("192.168.245.128:8848");
        String config = configService.getConfig("datasource.yml", "DEFAULT_GROUP", 1000);
        System.out.println(config);
    }

}
