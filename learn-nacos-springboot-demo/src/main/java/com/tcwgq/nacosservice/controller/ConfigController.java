package com.tcwgq.nacosservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2022/9/4 18:04
 */
@RestController
@RequestMapping("/config")
public class ConfigController {
    @Autowired
    private ConfigurableApplicationContext applicationContext;
    @Value("${spring.datasource.url}")
    private String url;

    @GetMapping("/get")
    public String get() {
        return url;
    }

    @GetMapping("/refresh")
    public String refresh() {
        return applicationContext.getEnvironment().getProperty("spring.datasource.url");
    }

    @GetMapping("/user")
    public String user() {
        return applicationContext.getEnvironment().getProperty("user.name")
                + "-" + applicationContext.getEnvironment().getProperty("user.age")
                + "-" + applicationContext.getEnvironment().getProperty("user.sex");
    }

}
